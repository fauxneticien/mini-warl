@lexer wrl_lexer

wrlItem   -> mainEntry subEntry:*

mainEntry ->
    "me" %line_data             #0, 1
         alternate:*            #2
         semanticDomain         #3
         definitionGroup # 4
         latinGroup:?   # 5
         englishGlossGroup:*    # 6
         appendices:*   # 7
         pdxGroup:* # 8
         exampleGroup:* # 9
         crossReferences:?  # 10
         comment:*  # 11
         sense:*    # 12
    "eme"   # 13

subEntry ->
    "sse" %line_data
         alternate:*
         semanticDomain
         definitionGroup
         latinGroup:?
         englishGlossGroup:*
         exampleGroup:*
         crossReferences:?
         comment:*        
         senseOfSubEntry:*
    "esse"

sense ->
    "se" %line_data:?
         alternate:*
         semanticDomain
         definitionGroup
         latinGroup:?
         englishGlossGroup:*
         exampleGroup:*
         crossReferences:?
         comment:*            
    "ese"

senseOfSubEntry ->
    "sub" %line_data:?
         alternate:*
         semanticDomain
         definitionGroup
         latinGroup:?
         englishGlossGroup:*
         exampleGroup:*
         crossReferences:?
         comment:*            
    "esub"

examplePair    ->
    "we" %line_data %source_info:?
         englishTranslation
    "ewe"
  | "wed" %line_data %source_info:?
          englishTranslation
    "ewed"
  | "wed" %line_data %source_info:?
          englishTranslation
    "ewed"

crossReferences ->
    compareWarlpiri:*
    synonym:*
    antonym:*
    compareSignLang:*
    compareOtherLang:*
    
# compareWarlpiriLines -> compareWarlpiri:+
# synonyms -> synonym:+
# antonyms -> antonym:+
# compareSignLangs -> compareSignLang:+
# compareOtherLangs -> compareOtherLang:+

pdxGroup ->
    ("pdx" | "pdxs") %line_data
    crossReference:?
    alternate:*
    semanticDomain:?
    definitionGroup:?
    englishGlossGroup:?
    exampleGroup:*
    crossReferences:?
    comment:*
    preverbList:?
    ("epdx" | "epdxs")

definitionGroup ->
    "def" %line_data
         comment:*
    "edef"

exampleGroup   ->
    "eg"
        comment:*
        examplePair:+
        comment:*
    "eeg"    
    
englishGlossGroup  ->
    oldEnglishGloss:?
    "gl"  %line_data
         comment:*
    "egl"
    
latinGroup  ->
    oldLatinGloss:?
    latinGloss

alternate          -> "alt"  %line_data "ealt"
antonym            -> "ant"  %line_data "eant"
appendices         -> "refa" %line_data "erefa"
comment            -> "cm"   %line_data "ecm"
compareWarlpiri    -> "cf"   %line_data "ecf"
compareOtherLang   -> "cmp"  %line_data "ecmp"
compareSignLang    -> "csl"  %line_data "ecsl"
crossReference     -> "xme"  %line_data "exme"
definition         -> "def"  %line_data "edef"
englishTranslation -> "et"   %line_data
latinGloss         -> "lat"  %line_data "elat"
oldEnglishGloss    -> "glo"  %line_data "eglo"
oldLatinGloss      -> "lato" %line_data "elato"
preverbList        -> "pvl"  %line_data "epvl"
semanticDomain     -> "dm"   %line_data "edm"
synonym            -> "syn"  %line_data "esyn"
warlpiriExample    -> ("we" | "wed")   %line_data

@{%

const moo = require('moo');

const wrl_lexer = moo.compile({
    newline:     { match: /[\r?\n]+/, lineBreaks: true },
    
    start_code:  { match: /\\[a-z|A-Z]+\s?/,
                   value: x => x.replace(/^\\/, '').replace(/\s$/, '') },
                   
    line_data:   { match: /[^\\]+/, lineBreaks: true,
                   value: x => x.replace(/\s[\n\t]*$/, '') },
                   
    source_info: { match: /\\\[[^\]]+\]\s?/, lineBreaks: true },
    
    end_code:    { match: /\s?\\e[a-z|A-Z]+$/,
                   value: x => x.replace(/^\s?\\/, '') }
});

%}